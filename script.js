// For1. a va b butun sonlari berilgan (a < b). a va b sonlari orasidagi barcha butun sonlarni (a va b ni ham) chiqaruvchi va chiqarilgan sonlar sonini chiqaruvchi programma tuzilsin. (a va b xam chiqarilsin).

// let a = 1;
// let b = 10;
// let count = 0;
// for (let i = a; i <= b; i++) {
//     console.log(i);
//     count++
// }
// console.log(`Chiqarilgan sonlar: ${count}`);


// For2. a va b butun sonlari berilgan (a < b). a va b sonlari orasidagi barcha butun sonlarni (a va b dan tashqari) kamayish tartibida chiqaruvchi va chiqarilgan sonlar sonini chiqaruvchi progma tuzilsin.
// let a = 1;
// let b = 10;
// let count = 0;

// for (let i = b - 1; i > a; i--) {
//   console.log(i);
//   count++;
// }
// console.log(`Chiqarilgan sonlar: ${count}`);


// For3. Bir kg konfetning narxi berilgan (haqiqiy son). 1, 2, 10 kg konfetni narxini chiqaruvchi programma tuzilsin.

// const nar_1_kg = 1000; 
// for (let i = 1; i <= 10; i++) {
//   const nar = i * nar_1_kg;
//   console.log(`${i} kg konfet narxi: ${nar} so'm`);
// }

// For4. Bir kg konfetning narxi berilgan (haqiqiy son). 1.2, 1.4, ..., 2 kg konfetni narxini chiqaruvchi programma tuzilsin.

// const nar_1_kg = 5.5; 
// for (let i = 1.2; i <= 2; i += 0.2) {
//   const nar = i * nar_1_kg;
//   console.log(i.toFixed(1) + ' kg konfet narxi: ' + (nar * i).toFixed(2) + ' so‘m');
// }

// For5. a va b butun sonlari berilgan (a < b). a dan b gacha bo'lgan barcha butun sonlar yig'indisini chiqaruvchi programma tuzilsin.

// let a = 1;
// let b = 10;
// let sum = 0;
// for (let i = a; i <= b; i++) {
//     sum += i;
// }
// console.log(sum);


// For6. a va b butun sonlari berilgan (a < b). a dan b gacha bo'lgan barcha butun sonlar ko'paytmasini chiqaruvchi programma tuzilsin.

// let a = 3;
// let b = 6;
// let sum = 1;
// for (let i = a; i <= b; i++) {
//     sum *= i;
// }
// console.log(sum);


// For7 a va b butun sonlari berilgan (a < b). a dan b gacha bo'lgan barcha butun sonlar kvadratlarining yig'indisini chiqaruvchi programma tuzilsin.

// let a = 3;
// let b = 6;
// let sum = 0;
// for (let i = a; i <= b; i++) {
//     sum += i*i;
// }
// console.log(sum);


// For8. n butun soni berilgan (n > 0). Quyidagi yig'indini hisoblovchi programma tuzilsin. S=1+1/2+1/3+...+1/n
// let n = 2; 
// let sum = 0;

// for (let i = 1; i <= n; i++) {
//   sum += 1/i;
// }
// console.log(sum);

// For9. n butun soni berilgan (n > 0). Quyidagi ko'paytmani hisoblovchi programma tuzilsin. S=1.1*1.2* 1.3*... *n

// let n = 2; 
// let sum = 1;
// for (let i = 1; i <= n; i++) {
//   sum *= (1 + (i / 10));
// }
// console.log(sum);


//For11. n butun soni berilgan (n > 0). Shu sonning kvadratini quyidagi formula asosida hisoblovchi programma tuzilsin.

// let n = 5;
// let sum = 0;
// for (let i = 1; i <= n; i++) {
//   sum += 2*i - 1;
//   console.log(i+"ning kvadrati ="+sum);
// }



//For12. n butun soni va a haqiqiy soni berilgan (n > 0). a ning n - darajasini aniqlovchi programma tuzilsin. a^n=a*a*a...a;

// let a = 3;
// let n = 5;
// let darajasi = 1;
// for (let i = 1; i <= n; i++) {
//   darajasi *= n;
//   console.log(a +" ning "+ n + "-darajasi " + darajasi);
// }


// For13. n butun soni va a haqiqiy soni berilgan (n > 0). Bir sikldan foydalanib a ning 1 dan n gacha bo'lgan barcha darajalarini chiqaruvchi programma tuzilsin.

// let n = 5;
// let a = 3;
// let res = 1;
// for(let i = 1; i <= n; i++) {
//   res *= a;
//   console.log(`${a} ning ${i}-darajasi: ${res}`);
// }


// For14. n butun soni va a haqiqiy soni berilgan (n > 0). Bir sikldan foydalanib quyidagi a ning 1 dan n gacha bo'lgan barcha darajalarini chiqaruvchi va yig'indini hisoblovchi programma tuzilsin. S = 1+a+a^2 + a^3 + ... a^n

// let n = 6; 
// let a = 2; 
// let su = 0;
// let lo = 1;
// for (let i = 0; i <= n; i++) {
//   su += lo;
//   lo *= a;
// }
// console.log("S =", su);


//For15. n butun soni berilgan (n > 0). Bir sikldan foydalangan holda quyidagi yig'indini hisoblovchi programma tuzilsin. S = 1! + 2! + 3! + ... +n!

// let n = prompt("n ni kiriting:");
// let su = 0;
// let factorial = 1;
// for (let i = 1; i <= n; i++) {
//   factorial *= i;
//   su += factorial;
// }
// console.log(su);


// For16. N va K butun sonlari berilgan. Quyidagi yig'indini chiqaruvchi programma tuzilsin.  S = 1^K+2^K+ 1^K + 2^K + ... + N^K

// let n = 5; 
// let a = 3; 
// let su = 0; 
// for(let i = 1; i <= n; i++){
//   su += Math.pow(i, a);
// }
// console.log(su);


// For17. N butun soni berilgan. Quyidagi yig'indini chiqaruvchi programma tuzilsin. S = 1^1 +2^2+ … + N^N

// let n = prompt("n sonini kiriting:");
// let sum = 0;
// for(let i = 1; i <= n; i++) {
//   sum += Math.pow(i, i);
// }
// console.log(sum);


// For18. A va B butun soni berilgan (A < B). A va B sonlari orasidagi barcha butun sonlarni chiqaruvchi programma tuzilsin. Bunda A soni 1 marta, (A + 1) soni 2 marta chiqariladi va xokazo.

// let A = 4; 
// let B = 10; 

// for(let i = A; i <= B; i++){
//   for(let j = 1; j <= (i == A ? 1 : 2); j++){
//     console.log(i);
//   }
// }


// For18. Sonning barcha bo’luvchilarini, ularning sonini va yig’indisini chiqaruvchi dastur tuzing.

// let n = 24;
// let sum = 0;
// console.log(` ${n} ning bo'luvchilari:`);
// for (let i = 1; i <= n; i++) {
//   if (n % i == 0) {
//     console.log(i); 
//     sum += i; 
//   }
// }
// console.log(` ${n} ning bo'luvchilari yig'indisi: ${sum}`); 


// For19. n butun soni berilgan (n > 1). N sonini tub yoki tub emasligini aniqlovchi programma tuzilsin.

// let n = 13; 
// let is = true;
// for(let i = 2; i < n; i++) {
//   if(n % i == 0) {
//     is = false;
//     break;
//   }
// }
// if(is) {
//   console.log(n + " soni tub");
// } else {
//   console.log(n + " soni tub emas");
// }


// Sonlarni quyidagi tartibda chiqaruvchi dastur tuzing. N = 5 bo’lganda,

// let n = 5;
// for(let i = 1; i <= n; i++){
//   let row = "";
//   for(let j=1; j<=i; j++){
//     row += j + " ";
//   }
//   console.log(row);
// }





//////////////////////////// 

//While1. A va B butun musbat sonlari berilgan (A> B). A usunlikdagi kesmada maksimal darajada B kesma joylashtirilgan. A kesmaning bo'sh qismini aniqlovchi programma tuzilsin. Ko'paytirish va bo'lish amallarini ishlatmang.

// let a = prompt("A ning kiriting");
// let b = prompt("B ning Kiriting");
// while (a > b) {
//   a--;
// }
// console.log((b - a - 1));


// While2. A va B butun musbat sonlari berilgan (A > B). A usunlikdagi kesmada B kesmadan nechta joylashtirish mumkinligini aniqlovchi programma tuzilsin. Ko'paytirish va bo'lish amallarini ishlatmang.

// let a = 15;
// let b = 3;
// let count = 0;
// while (a >= b) {
//   a -= b;
//   count++;
// }
// console.log("B kesmadan " + count + " marta joylashtirish mumkin.");


//While3. n butun soni berilgan (n > 0). Agar n soni 3 ning darajasi bo'lsa "3 - ning darajasi", aks xolda "3 - ning darajasi emas” degan natija chiqaruvchi programma tuzilsin. Qoldiqli bo'lish va bo'lish amallarini ishlatmang.

// let n = 27; 
// let daraja = 1;
// while (daraja <= n) {
//   if (Math.pow(3, daraja) === n) {
//     console.log("3 - ning darajasi");
//     break;
//   }
//   daraja++;
// }
// if (daraja > n) {
//   console.log("3 - ning darajasi emas");
// }


// While4. n va m butun musbat sonlari berilgan (n > m). n sonini m soniga bo'lib butun va qoldiq qismlarini bo'lish va qoldiqni olish amallarini ishlatmasdan topuvchi programma tuzilsin.

// let a = 15;
// let b = 4;
// let aq = 0;
// let qaa = 0;
// while (a >= b) {
//   a -= b;
//   aq++;
// }
// qaa = n;
// console.log(`${n} ni ${m} ga bo'lib qoldiq ${aq} va qoldiq ${qaa}`);



// While5. n butun soni berilgan (n > 0). Bo'lib butun va qoldiq qismlarini aniqlash orqali, berilgan son raqamlarini teskari tartibda chiqaruvchi programma tuzilsin.

// let n = 12345;
// let revers = 0;
// while (n > 0) {
//   revers = revers * 10 + n % 10;
//   n = Math.floor(n / 10);
// }
// console.log(revers);

// While6. n butun soni berilgan (n > 0). Bo'lib butun va qoldiq qismlarini aniqlash orqali, berilgan son raqamlari yig'indisini va raqamlari sonini chiqaruvchi programma tuzilsin.

// let n = 12345;
// let sum = 0;
// let count = 0;
// while (n > 0) {
//   let digit = n % 10;
//   sum += digit;
//   count++;
//   n = Math.floor(n / 10);
// }
// console.log("Raqamlar yig'indisi:", sum);
// console.log("Sonlar soni:", count);


// While7. n butun soni berilgan (n > 0). Bo'lib butun va qoldiq qismlarini aniqlash orqali, berilgan son raqamlarining orasida 2 raqami bor-yo'qligini aniqlovchi programma tuzilsin.

// let n = 12345;
// let Two = false;
// while (n > 0) {
//   let digit = n % 10;
//   if (digit === 2) {
//     Two = true;
//     break;
//   }
//   n = Math.floor(n / 10);
// }
// if (Two) {
//   console.log("2 raqami bor");
// } else {
//   console.log("2 raqami yo'q");
// }


// While8. n butun soni berilgan (n > 0). Bo'lib butun va qoldiq qismlarini aniqlash orqali, berilgan son raqamlarining orasida toq raqamlar bor-yo'qligini aniqlovchi programma tuzilsin.

// let n = 1234567;
// let hasOdd = false;
// while (n > 0) {
//   let digit = n % 10;
//   if (digit % 2 !== 0) {
//     hasOdd = true;
//     break;
//   }
//   n = Math.floor(n / 10);
// }
// if (hasOdd) {
//   console.log("toq raqamlar bor");
// } else {
//   console.log("toq raqamlar yo'q");
// }


// While9. Palindromik songa tekshirish dasturini yozing. True yoki False qaytarsin. Palindromik son – boshidan va oxiridan o’qilish bir xil bo’lgan son, masalan 1345431, 45788754

// let number = prompt("Son kiriting:");
// let originalNumber = number;
// let reverNumber = 0;
// while (number > 0) {
//   let digit = number % 10;
//   reverNumber = (reverNumber * 10) + digit;
//   number = Math.floor(number / 10);
// }

// if (originalNumber == reverNumber) {
//   console.log("True");
// } else {
//   console.log("False");
// }


// While10. n butun soni berilgan (n > 1). N sonini tub yoki tub emasligini aniqlovchi programma tuzilsin.

// let n = 17; 
// let isPr = true; 
// let i = 2; 
// while (i <= Math.sqrt(n)) {
//   if (n % i == 0) { 
//     isPr = false;
//     break; 
//   }
//   i++; 
// }
// if (isPr && n != 1) { 
//   console.log(n + " soni tub son");
// } else { 
//   console.log(n + " soni tub emas");
// }